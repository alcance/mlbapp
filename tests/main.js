describe('MainController', function() {

  var controller, scope;

  beforeEach(angular.mock.module('mlbApp'));

  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    controller = $controller('MainController', {
      $scope: scope
    });
  }));

  it('should have scope defined', function() {
    console.log('lel');
    expect(scope).toBeDefined();
  });

});