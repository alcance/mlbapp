module.exports = function(grunt) {
  //grunt wrapper function 
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    //grunt task configuration will go here
    uglify: {
      js: {
        src: ['./app/app.js', './app/controllers/main.js'],
        dest: './dist/app.min.js'
      }
    }
  });
  //load grunt tasks
  grunt.loadNpmTasks('grunt-contrib-uglify');

  //register grunt default task
  grunt.registerTask('default', ['uglify']);
}