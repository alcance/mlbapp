'use strict';

/**
 * @ngdoc function
 * @name mlbApp.controller:MainController
 * @description
 * # MainController
 * Controller of the App
 */

mlbApp.controller(
  'MainController',
  ['$scope', '$rootScope', 'GameService', 'moment', function($scope, $rootScope, GameService, moment) {
    $scope.title = 'Welcome to MLB Scoreboard App';
    $scope.date = moment().format('MMMM D, YYYY');

    $scope.prevDay = function() {
      $scope.date = moment($scope.date).subtract(1, 'days').format('MMMM D, YYYY');
    }

    $scope.nextDay = function() {
      $scope.date = moment($scope.date).add(1, 'days').format('MMMM D, YYYY');
    }

    // Watch datepicker change and fetch fresh data
    $scope.$watch('date', function(value) {
      $scope.error = '';
      GameService.getGames(value).then(function(results) {
        if (!results.data.data.games.game) {
          $scope.error = 'No games today';
          $scope.gameResults = '';
        } else {
          var game = results.data.data.games.game.find(function(game) {
            return game.home_team_name === 'Blue Jays' || game.away_team_name === 'Blue Jays';
          });
          if (game) {
            results.data.data.games.game.splice(results.data.data.games.game.indexOf(game), 1);
            results.data.data.games.game.unshift(game);
          }
          $scope.gameResults = results.data.data.games.game;
        }
      }).catch(function(err) {
        $scope.gameResults = '';
        $scope.error = 'No games today';
      });
    });
}]);