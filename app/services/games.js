'use strict';

/**
 * @ngdoc function
 * @name mlbApp.service:GameService
 * @description
 * # GameService
 * GameService to fetch game data
 */

mlbApp.factory('GameService', ['$http', 'moment', function($http, moment) {
  return {
    formatUrl: function(date) {
      var dateItems = moment(date).format('YYYY-DD-YY').split('-');
      return 'http://gd2.mlb.com/components/game/mlb/year_' + dateItems[0] + '/month_' + dateItems[1] + '/day_' + dateItems[2] + '/master_scoreboard.json';
    },
    getGames: function(value) {
      var url = this.formatUrl(value);
      return $http.get(url, {});
    }
  }
  
}]);