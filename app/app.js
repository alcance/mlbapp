'use strict';

/**
 * @ngdoc overview
 * @name mlbApp
 * @description
 * # mlbApp
 *
 * Main module of the application.
 */

var mlbApp = angular.module('mlbApp', ['ngRoute', '720kb.datepicker', 'angularMoment']);

//What to do when particular hash is set
mlbApp.config(function($routeProvider) {
  $routeProvider

  .when('/', {
    templateUrl: 'app/views/main.html',
    controller: 'MainController'
  })

});