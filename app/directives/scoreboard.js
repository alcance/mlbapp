mlbApp.directive('scoreboardCard', function () {
  return {
    restrict: "E",
    templateUrl: 'app/directives/partials/scoreboardCard.html',
  };
});